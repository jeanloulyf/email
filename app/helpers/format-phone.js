import Ember from 'ember';
import libPhoneNumber from 'libphonenumber';

const { PhoneNumberFormat } = libPhoneNumber;

const phoneUtil = libPhoneNumber.PhoneNumberUtil.getInstance();

export function formatPhone([number]) {
  if (number) {
    let phoneNumber = phoneUtil.parse(number, 'E164');
    return phoneUtil.format(phoneNumber, PhoneNumberFormat.INTERNATIONAL);
  }
}

export default Ember.Helper.helper(formatPhone);
