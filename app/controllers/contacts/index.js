import Ember from 'ember';

export default Ember.Controller.extend({
  contactSort: ['name:asc'],
  sortedContacts: Ember.computed.sort('model', 'contactSort')
});
