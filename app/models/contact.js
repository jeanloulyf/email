import Ember from 'ember';
import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { hasMany } from 'ember-data/relationships';

const { computed } = Ember;

export default Model.extend({
  name: attr('string'),
  contactEmails: hasMany('contact-email'),
  contactPhones: hasMany('contact-phone'),
  primaryPhone: computed('contactPhones.@each.primary', function() {
    return this.get('contactPhones').findBy('primary', true);
  }),
  primaryEmail: computed('contactEmails.@each.primary', function() {
    return this.get('contactEmails').findBy('primary', true);
  }),
  alternateEmailCount: computed('contactEmails.length', function() {
    return Math.max(this.get('contactEmails.length') - 1, 0);
  }),
  alternatePhoneCount: computed('contactPhones.length', function() {
    return Math.max(this.get('contactPhones.length') - 1, 0);
  })
});

