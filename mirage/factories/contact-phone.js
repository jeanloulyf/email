import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  number() {
    return faker.helpers.replaceSymbolWithNumber('+1555555555#');
  },
  primary: false
});
