export default function(server) {

  /*
    Seed your development database using your factories.
    This data will not be loaded in your tests.

    Make sure to define a factory for each model you want to create.
  */

  server.logging = true;
  let inbox = server.create('folder', { id: 'inbox', name: 'Inbox', userId: 1 });
  let inboxEmails = server.createList('email', 15, { folderId: inbox.id });

  server.db.folders.update(inbox.id, {
    emailCount: inboxEmails.length,
    unreadCount: server.db.emails.where({ folderId: inbox.id, read: false }).length
  });

  let spam = server.create('folder', { name: 'Spam', userId: 1, emailCount: 32, unreadCount: 13 });
  let spamEmails = server.createList('email', 32, { folderId: spam.id });

  let primaryEmail = server.create('contact-email', { address: 'smith@example.com', primary: true });
  let alternateEmail = server.create('contact-email', { address: 'smith@example.org' });
  let workEmail = server.create('contact-email', { address: 'asmith@example.edu' });
  let primaryPhone = server.create('contact-phone', { number: '+15555555252', primary: true });
  let alternatePhone = server.create('contact-phone', { number: '+15555550002' });

  let contact = server.create('contact', {
    name: 'Ann Smith',
    contactEmails: [primaryEmail, alternateEmail, workEmail],
    contactPhones: [primaryPhone, alternatePhone]
  });

  for (var i=0; i < 15; i++) {
    createContactWithDetails(server);
  }
}

function createContactWithDetails(server) {
  let emailCount = faker.random.number({ min: 0, max: 5 });
  let phoneCount = faker.random.number({ min: 0, max: 5 });

  let emails = server.createList('contact-email', emailCount);
  if (emails[0]) {
    emails[0].update({ primary: true });
  }

  let phones = server.createList('contact-phone', phoneCount);
  if (phones[0]) {
    phones[0].update({ primary: true });
  }

  let contact = server.create('contact', { contactEmails: emails, contactPhones: phones });
}
