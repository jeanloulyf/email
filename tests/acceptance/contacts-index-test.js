import { test, skip } from 'qunit';
import moduleForAcceptance from 'email/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | contacts index');

test('visiting user contacts route displays all contacts', function(assert) {
  server.createList('contact', 5);
  visit('/contacts');

  andThen(function() {
    assert.equal(find('.test-contact').length, 5, 'All contacts display');
  });
});

test('user contacts list is sorted by name', function(assert) {
  server.create('contact', { name: 'John Doe' });
  server.create('contact', { name: 'Jane Doe' });
  server.create('contact', { name: 'John Armstrong' });

  visit('/contacts');

  andThen(function() {
    assert.contains('.test-contact:eq(0) .test-contact-name', 'Jane Doe', 'Contact list is sorted alphabetically');
    assert.contains('.test-contact:eq(1) .test-contact-name', 'John Armstrong', 'Contact list is sorted alphabetically');
    assert.contains('.test-contact:eq(2) .test-contact-name', 'John Doe', 'Contact list is sorted alphabetically');
  });
});

test('displays info for a contact in each row', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith'
  });
  server.create('contact-email', { contactId: contact.id, address: 'smith@example.com', primary: true });
  server.create('contact-email', { contactId: contact.id, address: 'smith@example.org' });
  server.create('contact-email', { contactId: contact.id, address: 'asmith@example.edu' });
  server.create('contact-phone', { contactId: contact.id, number: '+15555555252', primary: true });
  server.create('contact-phone', { contactId: contact.id, number: '+15555550002' });

  visit('/contacts');

  andThen(function() {
    assert.contains('.test-contact:eq(0) .test-contact-name', 'Ann Smith', 'Contact row displays contact name');
    assert.contains('.test-contact:eq(0) .test-contact-primary-email', 'smith@example.com', 'Contact row displays contact primary email');
    assert.contains('.test-contact:eq(0) .test-contact-primary-email', '(+2)', 'Displays additional email count');
    assert.contains('.test-contact:eq(0) .test-contact-primary-phone', '+1 555-555-5252', 'Contact row displays primary phone');
    assert.contains('.test-contact:eq(0) .test-contact-primary-phone', '(+1)', 'Displays additional phone count');
  });
});

test('name links to edit for a contact', function(assert) {
  let contact = server.create('contact');

  visit('/contacts');

  click('.test-contact:eq(0) .test-contact-name a');

  andThen(function() {
    assert.equal(currentURL(), `/contacts/${contact.id}`, 'Now on contact page');
  });
});

test('primary email is a mailto: link', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith'
  });
  server.create('contact-email', { contactId: contact.id, address: 'smith@example.com', primary: true });

  visit('/contacts');

  andThen(function() {
    assert.equal(find('.test-contact-primary-email a').attr('href'), 'mailto:smith@example.com', 'Link to email is a mailto');
  });
});

test('primary phone is a tel: link', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith'
  });
  server.create('contact-phone', { contactId: contact.id, number: '+15555555252', primary: true });

  visit('/contacts');

  andThen(function() {
    assert.equal(find('.test-contact-primary-phone a').attr('href'), 'tel:+15555555252', 'Link to phone is a tel');
  });
});

test('With no emails or phones, none display', function(assert) {
  server.create('contact', {
    name: 'Ann Smith'
  });

  visit('/contacts');

  andThen(function() {
    assert.contains('.test-contact:eq(0) .test-contact-name', 'Ann Smith', 'Contact row displays contact name');
    assert.containsExactly('.test-contact:eq(0) .test-contact-primary-email', '', 'Primary email should not display');
    assert.containsExactly('.test-contact:eq(0) .test-contact-primary-phone', '', 'Primary phone should not display');
  });
});

skip('When no email is marked as primary, the first email displays');
skip('When no phone is marked as primary, the first phone displays');
